package com.droidbeginner.lutzps.justjava;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    int numeroDeCafes = 0;
    String name = "Lutz Santos";
    NumberFormat currencyBR = NumberFormat.getInstance(new Locale("pt", "BR"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button).setOnClickListener(this);
    }

    public void diminuirQuantidade(View view) {
        if (numeroDeCafes > 0) {
            mostrarQuantidader(--numeroDeCafes);
            mostrarPreco(numeroDeCafes * 5);
        } else {
            numeroDeCafes = 0;
        }
    }

    private void finalizarPedido() {
        TextView finalizarPedidoTextView = (TextView) findViewById(R.id.obrigado_texto);
        finalizarPedidoTextView.setText("Nome: " + name + "\nQuantidade: " + String.valueOf(numeroDeCafes)
                + "\nTotal: R$ " + (numeroDeCafes * 5) + "\nObrigado :)");
    }


    public void aumentarQuantidade(View view) {
        mostrarQuantidader(++numeroDeCafes);
        mostrarPreco(numeroDeCafes * 5);
    }

    private void mostrarQuantidader(int numeroDeCafes) {
        TextView mostrarQuantidadeTextView = (TextView) findViewById(R.id.valor_quantidade_texto);
        mostrarQuantidadeTextView.setText("" + numeroDeCafes);
    }

    private void mostrarPreco(int preco) {
        TextView mostrarPrecoTextView = (TextView) findViewById(R.id.preco_valor_view);
        mostrarPrecoTextView.setText("R$ " + preco);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.botao_aumentar_quantidade:
                aumentarQuantidade(view);
                break;
            case R.id.botao_diminur_quantidade:
                diminuirQuantidade(view);
                break;
            case R.id.button:
                finalizarPedido();
                break;
        }
    }
}